package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IAdminUserEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    public AdminUserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public User findByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.ADMIN);
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    @WebMethod
    public User lockByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.ADMIN);
        return serviceLocator.getUserService().lockByLogin(login);
    }

    @Override
    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @NotNull
    @Override
    @WebMethod
    public User unlockByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.ADMIN);
        return serviceLocator.getUserService().unlockByLogin(login);
    }

}
