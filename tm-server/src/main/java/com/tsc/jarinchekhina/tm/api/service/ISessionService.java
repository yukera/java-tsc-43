package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    @Nullable
    Session sign(@Nullable Session session);

    boolean isValid(@Nullable Session session);

    void validate(@Nullable Session session);

    void validate(@Nullable Session session, @Nullable Role role);

    @NotNull
    User getUser(@Nullable Session session);

    @NotNull
    String getUserId(@Nullable Session session);

    @NotNull
    List<Session> getListSession(@Nullable Session session);

    void add(@NotNull Session session);

    @NotNull
    Session findById(@NotNull String id);

    void removeById(@NotNull String id);

    void clear();

    void close(@Nullable Session session);

    void closeAll(@NotNull List<Session> sessionList);

}
