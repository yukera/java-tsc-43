package com.tsc.jarinchekhina.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Session extends AbstractBusinessEntity implements Cloneable {

    @Column
    @Nullable
    private Long timestamp;

    @Column
    @Nullable
    private String signature;

    @Nullable
    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
