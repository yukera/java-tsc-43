package com.tsc.jarinchekhina.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    SqlSession getSqlSession();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

}
