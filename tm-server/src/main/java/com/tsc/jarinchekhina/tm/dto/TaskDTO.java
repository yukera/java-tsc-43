package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.api.entity.IWBS;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
public class TaskDTO extends AbstractEntityDTO implements IWBS {

    @Column
    @NotNull
    private String userId;

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @Nullable
    private String projectId;

    @Column
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private Date dateFinish;

    @Column
    @Nullable
    private Date created = new Date();

    @Override
    public String toString() {
        return getId() + ": " + getName();
    }

}
