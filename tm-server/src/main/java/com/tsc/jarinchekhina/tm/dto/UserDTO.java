package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.enumerated.Role;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tm_user")
public class UserDTO extends AbstractEntityDTO {

    @Column
    @NotNull
    private String login;

    @Column
    @NotNull
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstName;

    @Column
    @Nullable
    private String lastName;

    @Column
    @Nullable
    private String middleName;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    private boolean locked = false;

}
