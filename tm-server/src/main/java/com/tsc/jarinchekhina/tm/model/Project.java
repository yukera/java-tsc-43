package com.tsc.jarinchekhina.tm.model;

import com.tsc.jarinchekhina.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
public class Project extends AbstractBusinessEntity {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private Date dateFinish;

    @Column
    @Nullable
    private Date created = new Date();

    @Nullable
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

}
