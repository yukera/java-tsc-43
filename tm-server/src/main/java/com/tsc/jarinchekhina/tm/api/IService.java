package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
